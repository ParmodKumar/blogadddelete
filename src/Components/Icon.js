import * as React from 'react';
import Icons from 'react-native-vector-icons/Ionicons';
import {Utils} from '../utills/utill';

const Icon = props => {
  const IconName = () => {
    if (Utils.IsIos) {
      return 'ios-' + props.name;
    }
    return 'md-' + props.name;
  };
  return (
    <Icons
      onPress={props.onPress}
      name={props.noPrefix ? props.name : IconName()}
      color={props.color}
      size={props.size}
      style={props.style}
    />
  );
};

Icon.defaultProps = {
  noPrefix: false,
  color: '#fff',
};
export default Icon;
