import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import IndexScreen from '../../Screen/IndexScreen';
import ShowScreens from '../../Screen/ShowScreen';
import NavigationString from './NavigationString';
import CreateScreen from '../../Screen/CreateScreen';
import EditScreen from '../../Screen/EditScreen';
const Stack = createNativeStackNavigator();

export const authStack = () => {
  return (
    <Stack.Navigator initialRouteName={NavigationString.IndexScreen}>
      <Stack.Screen
        name={NavigationString.IndexScreen}
        component={IndexScreen}
      />
      <Stack.Screen
        name={NavigationString.ShowScreens}
        component={ShowScreens}
      />
      <Stack.Screen
        name={NavigationString.CreateScreen}
        component={CreateScreen}
      />
      <Stack.Screen name={NavigationString.EditScreen} component={EditScreen} />
    </Stack.Navigator>
  );
};
