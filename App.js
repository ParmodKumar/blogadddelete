// import React from 'react';

import React from 'react';
import {Navigator} from './src/utills/Navigations/Navigator';
import {Provider} from './src/context/BlogContext';
const App = () => {
  return (
    <Provider>
      <Navigator />
    </Provider>
  );
};

export default App;
