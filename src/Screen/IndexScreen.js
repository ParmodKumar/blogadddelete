import React, {useContext, useEffect} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  FlatList,
  Button,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import {Context} from '../context/BlogContext';
import Icon from '../Components/Icon';
import NavigationString from '../utills/Navigations/NavigationString';
import {useNavigation} from '@react-navigation/native';

const IndexScreen = ({navigation}) => {
  let nav = useNavigation();

  useEffect(() => {
    nav.setOptions({
      headerRight: () => (
        <TouchableOpacity onPress={() => navigation.navigate('Create')}>
          <Icon name="add" size={30} color="#900" />
        </TouchableOpacity>
      ),
    });
  }, [nav]);

  // const blogPost = useContext(BlogContext);
  const {state, deleteBlogPost, getBlogPosts} = useContext(Context);
  useEffect(() => {
    getBlogPosts();
    const listener = navigation.addListener('didFocus', () => {
      getBlogPosts();
    });
    return () => {
      listener.remove;
    };
  });
  // const money = 500;
  return (
    <SafeAreaView>
      <View>
        <Text>IndexScreen</Text>
        {/* <Icon name="add" size={30} color="red" /> */}

        <FlatList
          data={state}
          keyExtractor={blogPost => blogPost.title}
          renderItem={({item}) => {
            return (
              <TouchableOpacity
                onPress={() =>
                  navigation.navigate(NavigationString.ShowScreens, {
                    id: item.id,
                    // title: item.title,
                  })
                }>
                <View style={styles.row}>
                  <Text style={styles.title}>
                    {item.title} {item.id}
                  </Text>
                  <TouchableOpacity onPress={() => deleteBlogPost(item.id)}>
                    <Icon
                      style={styles.icon}
                      name="trash"
                      size={30}
                      color="#900"
                    />
                  </TouchableOpacity>
                </View>
              </TouchableOpacity>
            );
          }}
        />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 20,
    paddingHorizontal: 10,
    borderTopWidth: 1,
    borderColor: 'gray',
  },
  title: {
    fontSize: 18,
  },
  icon: {
    fontSize: 24,
  },
});

export default IndexScreen;

// onPress={() => navigation.navigate('Api')}
