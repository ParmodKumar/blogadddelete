import React, {useContext, useEffect, useState} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {Context} from '../context/BlogContext';
import {useNavigation} from '@react-navigation/native';
import Icon from '../Components/Icon';
import NavigationString from '../utills/Navigations/NavigationString';
const ShowScreen = ({route, navigation}) => {
  let pencil = useNavigation();
  // navigator
  useEffect(() => {
    pencil.setOptions({
      headerRight: () => (
        <TouchableOpacity
          onPress={() =>
            navigation.navigate(NavigationString.EditScreen, {
              id: route.params.id,
            })
          }>
          <Icon name="pencil" size={30} color="#900" />
        </TouchableOpacity>
      ),
    });
  }, [pencil]);

  const {state} = useContext(Context);
  const blogPost = state.find(blogPost1 => blogPost1.id === route.params.id);
  return (
    <View>
      <Text>ShowScreen</Text>
      {/* <Text>{id}</Text> */}
      <Text>{blogPost.title}</Text>
      <Text>{blogPost.content}</Text>
    </View>
  );
};

export default ShowScreen;

// const [titles, settitle] = useState({});
// // const {itemId, otherParam} = route.params;
// console.log(({id, title} = route.params));
// const {id, title} = route.params;
// console.log(id, title);
