import React, {useState, useContext} from 'react';
import {View, Text, TextInput} from 'react-native';
import {Context} from '../context/BlogContext';
import BlogPostForm from '../Components/BlogPostForm';
const EditScreen = ({route, navigation}) => {
  console.log(navigation);
  const id = route.params.id;
  const {state, editBlogPost} = useContext(Context);
  const blogPost = state.find(blogPost1 => blogPost1.id === id);
  // const [title, settitle] = useState(blogPost.title);
  // const [content, setcontent] = useState(blogPost.content);

  return (
    <BlogPostForm
      initailValues={{title: blogPost.title, content: blogPost.content}}
      onSubmit={(title, content) => {
        editBlogPost(id, title, content, () => navigation.pop());
      }}
    />
  );
};

export default EditScreen;
