import React, {useContext} from 'react';
import {StyleSheet} from 'react-native';
import BlogPostForm from '../Components/BlogPostForm';
import {Context} from '../context/BlogContext';
import NavigationString from '../utills/Navigations/NavigationString';

const CreateScreen = ({navigation}) => {
  const {addBlogPost} = useContext(Context);

  return (
    <React.Fragment>
      <BlogPostForm
        onSubmit={(title, content) => {
          addBlogPost(title, content, () => {
            navigation.navigate(NavigationString.IndexScreen);
          });
        }}
      />
    </React.Fragment>
  );
};

const styles = StyleSheet.create({});

export default CreateScreen;
