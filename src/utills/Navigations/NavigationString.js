export default {
  IndexScreen: 'Index',
  ShowScreens: 'Show',
  CreateScreen: 'Create',
  EditScreen: 'Edit',
};
