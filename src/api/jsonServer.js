import axios from 'axios';

export default axios.create({
  baseURL: 'http://5bd9-103-200-84-71.ngrok.io',
});

// 192.168.1.103.
// http://192.168.1.103:3000/api/houses'
